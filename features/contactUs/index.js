import React from "react";
import useStyles from "./styles";
import Grid from "@material-ui/core/Grid/Grid";
import CustomContainer from "../../components/Container";
import {Button, TextField} from "@material-ui/core";

const ContactUsPage = () => {
  const classes = useStyles({
    // width:  window.screen.width,
    // height: window.screen.height,
  });

  return (
    <div className={classes.contactUsWrapper}>
      <section id="getInTouch" className={classes.getInTouchSection}>
        <CustomContainer>
          <Grid container>
            <Grid item {...{ md: 12, sm: 12 }} style={{ width: "100%" }}>
              <div className={classes.sectionTitle}>
                <img src={"/images/email.svg"} />
                <p>Get in Touch With Solio</p>
              </div>
            </Grid>
            <Grid item {...{ md: 5 }}>
              <div className={classes.generalInformationBox}>
                <div className={classes.sectionTitle}>General Information</div>
                <div>
                  <div className={classes.generalInformationItem}>
                    <div className={classes.label}>Solio Main Office</div>
                    <div className={classes.innerText}>
                      Office 12C, Dubai Creek Tower, Deira, Dubai, UAE
                    </div>
                  </div>

                  <div className={classes.generalInformationItem}>
                    <div className={classes.label}>Phone Number</div>
                    <div className={classes.innerText}>+1 650 433 9005</div>
                  </div>

                  <div className={classes.generalInformationItem}>
                    <div className={classes.label}>
                      Question or Concerns Mail
                    </div>
                    <div className={classes.innerText}>info@solio.site</div>
                  </div>

                  <div className={classes.generalInformationItem}>
                    <div className={classes.label}>Influencers Contact</div>
                    <div className={classes.innerText}>
                      influencers@solio.site
                    </div>
                  </div>

                  <div className={classes.generalInformationItem}>
                    <div className={classes.label}>Merchants Contact</div>
                    <div className={classes.innerText}>
                      merchants@solio.site
                    </div>
                  </div>
                </div>
              </div>
            </Grid>

            <Grid item {...{ md: 7 }}>
              <div className={classes.formWrapper}>
                <div className={classes.formTitle}>Contact Form</div>
                <Grid container>
                  <Grid item {...{md: 5, sm: 12}}>
                    <div>Full Name</div>
                    <TextField
                      variant={"outlined"}
                      placeholder={"Your Name"}
                    />
                  </Grid>

                  <Grid item {...{md: 12}}>
                    <div>Subject</div>
                    <TextField
                      variant={"outlined"}
                    />
                  </Grid>

                  <Grid item {...{md: 12}}>
                    <div>Your Message</div>
                    <TextField
                      multiline
                      variant={"outlined"}
                    />
                  </Grid>
                </Grid>

                <div>
                  <Button className={classes.submitBtn} variant={"contained"}>Submit</Button>
                </div>
              </div>
            </Grid>
          </Grid>
        </CustomContainer>
      </section>
      <section id="download" className={classes.downloadSection}>
        <CustomContainer className={classes.downloadSectionContent}>
          <img src={"/images/logo-square.png"} />

          <p className={classes.sectionTitle}>Ready to Try it?</p>
          <p className={classes.innerText}>
            Solio is a new collaborative shopping experience!
          </p>
          <div className={classes.downloadBtn}>
            <img src={"/images/google-play-badge.svg"} />
          </div>
          <img className={"samsung"} src={"/images/samsung-black.svg"} />
        </CustomContainer>
      </section>
    </div>
  );
};

export default ContactUsPage;

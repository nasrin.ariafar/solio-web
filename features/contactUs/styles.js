import {makeStyles} from "@material-ui/core";

export default makeStyles((theme) => ({
  contactUsWrapper: {
    "& section": {
      width: "100%",
      "&:last-child": {
        // paddingBottom: 0
      },
      "& .MuiGrid-item": {
        padding: "10px",
      },
      "& .MuiContainer-root": {
        padding: "120px 0",
        [theme.breakpoints.down("md")]: {
          padding: "80px 0",
        },
      },
    },
  },
  sectionTitle: {
    fontSize: "40px",
    lineHeight: "54px",
    fontWeight: 800,
    [theme.breakpoints.down("md")]: {
      fontSize: "28px",
      lineHeight: "40px",
    },
  },
  sectionTitle2: {
    fontSize: "48px",
    lineHeight: "64px",
    fontWeight: 800,
  },

  text: {
    fontSize: "18px",
    lineHeight: "32px",
    maxWidth: "390px",
  },
  innerText: {
    fontSize: "16px",
    lineHeight: "26px",
    color: theme.palette.black.light,
  },

  centerAlign: {
    textAlign: "center",
  },
  getInTouchSection: {
    backgroundImage: "linear-gradient(180deg, #C2F0FF 0%, #FFFFE7 100%);",
    "& $sectionTitle": {
      textAlign: "center",
      "& p": {
        color: theme.palette.blue.main,
        marginTop: "-10px",
      },
    },
  },
  generalInformationBox: {
    maxWidth: "372px",
    "& $sectionTitle": {
      color: theme.palette.black.light,
      textAlign: "left",
      lineHeight: "40px",
      fontSize: "28px",
      marginBottom: "10px",
    },
  },
  label: {
    color: theme.palette.black.light,
    fontWeight: "500",
    lineHeight: "30px",
    fontSize: "20px",
  },
  generalInformationItem: {
    padding: "20px 0",
    borderBottom: "1px solid rgba(0,0,0,0.1)",
    "&:last-child": {
      borderBottom: "none",
    },

    "& $innerText": {
      color: theme.palette.black.light,
      fontWeight: "normal",
      fontSize: "16px",
      lineHeight: "26px",
    },
  },
  formWrapper: {
    backgroundColor: "#ffffff",
    padding: "40px",
    border: "1px solid #D9DBE1",
    boxSizing: "border-box",
    borderRadius: "8px",
    "& $label": {
      fontWeight: "700",
      lineHeight: "24px",
      fontSize: "14px",
    },
  },
  formTitle:{
    color: theme.palette.black.light,
    fontWeight: "600",
    lineHeight: "32px",
    fontSize: "24px",
  },
  downloadSection: {
    backgroundImage:
      "linear-gradient(180deg, #E7E7FF 0%, #FFFEE7 0.01%, #FFFFFF 68.7%);",
    // padding: "120px 100px 0 100px !important",
    [theme.breakpoints.down("md")]: {
      padding: "0 !important",
    },
    "& > div": {
      backgroundImage: "url(/images/download-section-bg.png)",
      position: "relative",
      backgroundSize: "contain",
      backgroundPosition: "bottom",
      backgroundRepeat: "no-repeat",
      width: "100%",
      height: "auto",
      textAlign: "center",
      overflow: "hidden",
      [theme.breakpoints.down("md")]: {
        backgroundSize: "100% 50%",
      },
      "& img.samsung": {
        marginBottom: "-10%",
        marginTop: "20px",
        width: "350px",
        // top: "410px",
        // position: "relative"
      },
    },
    "& $sectionTitle": {
      color: theme.palette.blue.main,
      lineHeight: "30px",
    },
    "& $bg": {},
  },
  downloadSectionContent: {
    textAlign: "center",
  },
  submitBtn:{
    color:"white",
    background: "#4945FF",
    borderRadius: "8px",
    width: "135px",
    height: "40px"
  }
}));

import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  landingWrapper: {
    "& section": {
      width: "100%",
      "&:last-child": {
        // paddingBottom: 0
      },
      "& .MuiGrid-item": {
        padding: "10px",
      },
      "& .MuiContainer-root": {
        padding: "120px 0",
        [theme.breakpoints.down("md")]: {
          padding: "20px",
        },
      },
    },
  },
  homeSection: {
    display: "flex",
    paddingTop: 50,
    [theme.breakpoints.down("md")]: {
      paddingTop: 280,
    },
  },
  sectionTitle: {
    fontSize: "40px",
    lineHeight: "54px",
    fontWeight: 800,
    [theme.breakpoints.down("md")]: {
      fontSize: "28px",
      lineHeight: "40px",
    },
  },
  sectionTitle2: {
    fontSize: "48px",
    lineHeight: "64px",
    fontWeight: 800,
  },
  image1: {
    height: "650px",
    width: "auto",
    [theme.breakpoints.down("md")]: {
      height: "375px",
    },
    marginLeft: "auto",
    position: "absolute",
    top: 0,
    right: 0,
  },
  text: {
    fontSize: "18px",
    lineHeight: "32px",
    maxWidth: "390px",
  },
  innerText: {
    fontSize: "16px",
    lineHeight: "26px",
    color: theme.palette.black.light,
  },
  featureItem: {
    display: "flex",
    alignItems: "center",
    marginBottom: "30px",
    "& p": {
      margin: 0,
    },
    "& img": {
      marginRight: "28px",
    },
    "&.reverse": {
      [theme.breakpoints.up("md")]: {
        flexDirection: "row-reverse",
        "& div": {
          textAlign: "right",
          marginRight: "28px",
        },
      },
    },
  },
  featureCarouselItem: {
    padding: "20px 50px",
    textAlign: "center",
  },
  sectionBigImage: {
    display: "block",
    margin: "auto",
    width: "90%",
  },
  featureTitle: {
    fontSize: "20px",
    fontWeight: "500",
  },
  featureSubTitle: {
    fontSize: "16px",
  },
  centerAlign: {
    textAlign: "center",
  },
  shopSubtitle: {
    fontSize: "18px",
    lineHeight: "32px",
    textAlign: "center",
    width: "65%",
    margin: "auto",
    marginBottom: "24px",
  },
  featuresWrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    [theme.breakpoints.down("md")]: {
      display: "block",
    },
  },
  shopItemsRow: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "& img": {
      width: 100 / 8 + "%",
      [theme.breakpoints.down("md")]: {
        width: 100 / 3 + "%",
      },
    },
    [theme.breakpoints.down("md")]: {
      "& img:nth-child(n+4)": {
        display: "none",
      },
    },
  },
  innerTitle: {
    fontSize: "24px",
    lineHeight: "32px",
    fontWeight: 600,
    "&.orange": {
      color: theme.palette.orange.main,
    },
    "&.pink": {
      color: theme.palette.pink.main,
    },
    "&.purple": {
      color: theme.palette.purple.main,
    },
  },
  iconCircleWrapper: {
    width: "72px",
    height: "72px",
    background: "#F4F5F7",
    borderRadius: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginRight: "20px",
    textAlign: "center",
    "& img": {
      height: "100%",
      padding: "20px",
      margin: "auto",
    },
  },
  exploreSection: {
    "& $sectionTitle": {
      color: theme.palette.orange.main,
    },
  },
  iconTitle: {
    display: "flex",
    alignItems: "center",
    "& p": {
      marginLeft: "-20px",
    },
  },
  watchSection: {
    [theme.breakpoints.down("md")]: {
      backgroundImage:
        "linear-gradient(180deg, rgba(255, 243, 179, 0.6) 0%, #FFFFFF 43.34%);",
    },
    [theme.breakpoints.up("md")]: {
      backgroundImage:
        "linear-gradient(90deg, rgba(255, 215, 1, 0.3) 0%, rgba(255, 215, 1, 0) 73.27%);",
    },
    "& > div": {
      [theme.breakpoints.down("md")]: {
        flexDirection: "column-reverse",
      },
    },
    "& $sectionTitle": {
      color: theme.palette.yellow.main,
      marginLeft: "-20px",
    },
  },
  shopSection: {
    [theme.breakpoints.down("md")]: {
      backgroundImage: "linear-gradient(180deg, #F4F1FF 0%, #FFFFFF 66.83%);",
    },
    [theme.breakpoints.up("md")]: {
      backgroundImage:
        "linear-gradient(330.18deg, rgba(112, 62, 255, 0) 52.64%, rgba(112, 62, 255, 0.15) 100%);",
    },
    "& $sectionTitle": {
      textAlign: "center",
      "& p": {
        color: theme.palette.blue.main,
        marginTop: "-40px",
      },
    },
    "& $featureItem": {
      padding: "24px 40px 75px",
    },
  },
  featuresSection: {
    backgroundImage: "linear-gradient(180deg, #C2F0FF 0%, #FFFFE7 100%);",
    "& $sectionTitle": {
      color: theme.palette.blue.main,
      [theme.breakpoints.down("md")]: {
        fontSize: "40px",
        lineHeight: "54px",
        textAlign: "center",
      },
    },
    "& $featureItem": {
      border: `1px solid ${theme.palette.blue.light1}`,
      borderRadius: "24px",
      padding: "9px 20px",
      display: "flex",
      width: "100%",
      marginBottom: "12px",
    },
  },
  mockup: {
    backgroundImage: "url('/images/phone-mockup.png')",
    width: "420px",
    height: "663px",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    margin: "auto",
  },
  innerMockup:{
    width: "244.15px",
    height: "540.49px",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat",
    margin: "auto",
    backgroundPosition: "0px 41px",
    borderRadius: "20px",
  },
  innerMockup1: {
    backgroundImage: "url('/images/feature-1-image.png')",
  },
  innerMockup2: {
    backgroundImage: "url('/images/feature-2-image.png')",
  },
  innerMockup3: {
    backgroundImage: "url('/images/feature-3-image.png')",
  },
  innerMockup4: {
    backgroundImage: "url('/images/feature-4-image.png')",
  },
  downloadSection: {
    backgroundImage:
      "linear-gradient(180deg, #E7E7FF 0%, #FFFEE7 0.01%, #FFFFFF 68.7%);",
    // padding: "120px 100px 0 100px !important",
    [theme.breakpoints.down("md")]: {
      padding: "0 !important",
    },
    "& > div": {
      backgroundImage: "url(/images/download-section-bg.png)",
      position: "relative",
      backgroundSize: "contain",
      backgroundPosition: "bottom",
      backgroundRepeat: "no-repeat",
      width: "100%",
      height: "auto",
      textAlign: "center",
      overflow: "hidden",
      [theme.breakpoints.down("md")]: {
        backgroundSize: "100% 50%",
      },
      "& img.samsung": {
        marginBottom: "-35%",
        // top: "410px",
        // position: "relative"
      },
    },
    "& $sectionTitle": {
      color: theme.palette.blue.main,
      lineHeight: "30px",
    },
    "& $bg": {},
  },
  downloadSectionContent: {
    textAlign: "center",
  },
  bgImage: {
    // visibility: "hidden",
    // position: "absolute",
    // top: 35,
    // left: 0,
    // maxWidth: "100%"
    // height:"490px"
  },
  featureSectionYellowBox: {
    background: "#FFE55A",
    borderRadius: "16px",
    flex: "none",
    order: 1,
    flexGrow: 0,
    padding: "48px",
    marginTop: "70px",
    "& $sectionTitle": {
      color: "#18191F",
      lineHeight: "40px",
      fontSize: "28px",

      [theme.breakpoints.down("md")]: {
        fontSize: "18px",
        lineHeight: "20px",
      },
    },
    "& $innerText": {
      fontSize: "18px",
    },
    [theme.breakpoints.down("md")]: {
      padding: "24px",
      paddingBottom: "55%",
      backgroundPosition: "bottom",
      "& *": {
        textAlign: "left",
      },
    },
    backgroundImage: "url('/images/accesibility.png')",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "right",
    margin: "auto",
    backgroundSize: "contain",
    "& > div:first-child": {
      position: "relative",
      "&:after": {
        content: "''",
        backgroundImage: "url('/images/curved-arrow.png')",
        width: "180px",
        height: "150px",
        position: "absolute",
        top: 0,
        right: "-30px",
        backgroundRepeat: "no-repeat",
        backgroundPosition: "100% 100%",
        backgroundSize: "contain",
        [theme.breakpoints.down("md")]: {
          display: "none",
        },
      },
    },
  },
  // designatedletter
  sidesSections: {
    padding: "64px 100px",
    backgroundColor: "#18191F",
    [theme.breakpoints.down("md")]: {
      padding: "30px 20px 30px 20px",
    },
    "& $sectionTitle": {
      color: "#F4F5F7",
      textAlign: "center",
      lineHeight: "40px",
      fontSize: "28px",
    },
    "& .MuiGrid-item": {
      padding: "10px",
      "& > div": {
        height: "100%",
        backgroundColor: "white",
        borderRadius: "8px",
        textAlign: "center",
        padding: "20px",
      },
    },
    "& .MuiContainer-root": {
      padding: "0",
      // [theme.breakpoints.down("md")]: {
      //   padding: "20px",
      // },
    },
  },
  whiteCentralTest: {
    textAlign: "center",
    margin: "auto",
    color: "white",
    marginTop: "10px",
    "& button": {
      color: "white",
      border: "1px solid #FFFFFF",
      borderRadius: "8px",
      padding: "5px 20px",
      margin: "10px auto",
    },
  },
}));

import React from "react";

export default [{
  icon: <img src={"/images/feature-icon.svg"}/>,
  title: "Curated Lists & Easy Search",
  image: <img src={"/images/feature-1-image.png"}/>
},{
  icon:   <img src={"/images/search-icon.svg"}/>,
  title: "Instant Collaboration",
  image: <img src={"/images/feature-2-image.png"}/>
},{
  icon: <img src={"/images/tag.svg"}/>,
  title: "Tagged Products",
  image: <img src={"/images/feature-3-image.png"}/>
},{
  icon:  <img src={"/images/wishlist-icon.svg"}/>,
  title: "Quick Buylist",
  image: <img src={"/images/feature-1-image.png"}/>
}]
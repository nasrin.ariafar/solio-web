import React from "react";
import useStyles from "./styles";
import Grid from "@material-ui/core/Grid/Grid";
import { Button, Hidden, Typography } from "@material-ui/core";
import clsx from "clsx";
import features from "./features";
import Carousel from "../../components/Carousel";
import Container from "@material-ui/core/Container/Container";
import CustomContainer from "../../components/Container";
import { purple } from "@material-ui/core/colors";

const LandingPage = () => {
  const classes = useStyles({
    // width:  window.screen.width,
    // height: window.screen.height,
  });

  return (
    <div className={classes.landingWrapper}>
      <section id="home" className={classes.homeSection}>
        <CustomContainer>
          <Grid container>
            <Grid item {...{ md: 6, sm: 12 }}>
              <div className={classes.section1Content}>
                <div className={classes.section1ContentWrapper}>
                  <Hidden mdDown>
                    <div>
                      <img width={92} src={"/images/logo-square.png"} />
                    </div>
                  </Hidden>

                  <div>
                    <img width={"80%"} src={"/images/watch-text.svg"} />
                  </div>
                  <div>
                    <p className={classes.text}>
                      Solio Videos and Lives help you find the right product and
                      buy them quickly while having fun. We also make these
                      videos with accessibility features in mind from day one.
                    </p>
                  </div>
                </div>
              </div>
            </Grid>
            <Grid item {...{ md: 6, sm: 12 }}>
              <img
                width={"100%"}
                className={classes.image1}
                src={"/images/creative-block.png"}
              />
            </Grid>
          </Grid>
        </CustomContainer>
      </section>

      <section id="watch" className={classes.watchSection}>
        <CustomContainer>
          <Grid container>
            <Grid item {...{ md: 6, sm: 12 }}>
              <img width={"100%"} src={"/images/watch-section-image.png"} />
            </Grid>
            <Grid item {...{ md: 6, sm: 12 }}>
              <div className={classes.iconTitle}>
                <img src={"/images/eye.svg"} />
                <p className={classes.sectionTitle}>Watch easily,</p>
              </div>
              <div>
                <p className={classes.text}>
                  Search and find your desired products and video reviews or
                  deep dive in to Solio’s curated list .
                </p>

                <div>
                  <div className={classes.featureItem}>
                    <img src={"/images/list-icon.svg"} />
                    <div>
                      <p className={classes.featureTitle}>
                        Curated Lists Just For You
                      </p>
                      <p className={classes.featureSubTitle}>
                        Discover suggestions based on your activity,
                        preferences, and desired products.
                      </p>
                    </div>
                  </div>

                  <div className={classes.featureItem}>
                    <img src={"/images/search-icon.svg"} />
                    <div>
                      <p className={classes.featureTitle}>
                        Easy search & exploration{" "}
                      </p>
                      <p className={classes.featureSubTitle}>
                        Find videos and products without any trouble with our
                        excellent search system.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </Grid>
          </Grid>
        </CustomContainer>
      </section>

      <section id="explore" className={classes.exploreSection}>
        <CustomContainer>
          <Grid container>
            <Grid item {...{ md: 6, sm: 12 }}>
              <div className={classes.iconTitle}>
                <img width={80} src={"/images/navigator.svg"} />
                <p className={classes.sectionTitle}>Communicate with Fun!</p>
              </div>
              <div>
                <p className={classes.text}>
                  Ask questions and get answers in real-time while watching live
                  videos. Tell your feedbacks & help others find their ideals
                  while having fun.
                </p>

                <div>
                  <div className={classes.featureItem}>
                    <img src={"/images/collaboration-icon.svg"} />
                    <div>
                      <p className={classes.featureTitle}>
                        Instant Collaboration
                      </p>
                      <p className={classes.featureSubTitle}>
                        You can ask streamers to show you the
                      </p>
                      <p>products in different sizes and colors,</p>
                      <p>or even try them on!</p>
                    </div>
                  </div>

                  {/*<div className={classes.featureItem}>*/}
                  {/*  <img src={'/images/feature-icon.svg'}/>*/}
                  {/*  <div>*/}
                  {/*    <p className={classes.featureTitle}>Feature 2</p>*/}
                  {/*    <p className={classes.featureSubTitle}>One line description</p>*/}
                  {/*  </div>*/}
                  {/*</div>*/}
                </div>
              </div>
            </Grid>

            <Grid item {...{ md: 6, sm: 12 }}>
              <img
                className={classes.sectionBigImage}
                src={"/images/explore.png"}
              />
            </Grid>
          </Grid>
        </CustomContainer>
      </section>

      <section id="shop" className={classes.shopSection}>
        <CustomContainer>
          <div className={classes.sectionTitle}>
            <img src={"/images/basket.svg"} />
            <p>Shop Quickly</p>
          </div>

          <p className={classes.shopSubtitle}>
            While watching personalized and fun videos, you can also see tagged
            products without leaving the video & buy them quickly.
          </p>

          <div className={classes.featuresWrapper}>
            <div className={clsx(classes.featureItem, "reverse")}>
              <img src={"/images/tag.svg"} />
              <div>
                <p className={classes.featureTitle}>Product Tagging</p>
                <p className={classes.featureSubTitle}>
                  Find the presented products in the videos with product tags.
                </p>
              </div>
            </div>

            <div className={classes.featureItem}>
              <img src={"/images/wishlist-icon.svg"} />
              <div>
                <p className={classes.featureTitle}>Quick Wishlist</p>
                <p className={classes.featureSubTitle}>
                  Add products to your wishlist without stopping the video.
                </p>
              </div>
            </div>
          </div>

          <div className={classes.shopItemsRow}>
            <img src={"/images/logo-1.png"} />
            <img src={"/images/logo-2.png"} />
            <img src={"/images/logo-3.png"} />
            <img src={"/images/logo-4.png"} />
            <img src={"/images/logo-5.png"} />
            <img src={"/images/logo-6.png"} />
            <img src={"/images/logo-7.png"} />
            <img src={"/images/logo-8.png"} />
          </div>

          <div className={classes.shopItemsRow}>
            <img src={"/images/logo-9.png"} />
            <img src={"/images/logo-10.png"} />
            <img src={"/images/logo-11.png"} />
            <img src={"/images/logo-12.png"} />
            <img src={"/images/logo-13.png"} />
            <img src={"/images/logo-14.png"} />
            <img src={"/images/logo-15.png"} />
          </div>
        </CustomContainer>
      </section>

      <section id="features" className={classes.featuresSection}>
        <CustomContainer>
          <Grid container>
            <Grid item {...{ md: 6, sm: 12 }} style={{ width: "100%" }}>
              <p className={classes.sectionTitle}>Features at a Glance</p>

              <Hidden mdDown>
                <div>
                  {features.map((item, index) => (
                    <div key={index} className={classes.featureItem}>
                      <div className={classes.iconCircleWrapper}>
                        {item.icon}
                      </div>
                      <div>
                        <p className={classes.innerTitle}> {item.title} </p>
                        {/*<p className={classes.innerText}>*/}
                        {/*  {item.description}*/}
                        {/*</p>*/}
                      </div>
                    </div>
                  ))}
                </div>
              </Hidden>

              <Hidden mdUp>
                <div style={{ width: "100%" }}>
                  <Carousel itemsToShow={1} itemWidth={100} items={features}>
                    {features.map((item, index) => (
                      <div key={index} style={{ width: "100%" }}>
                        <div className={classes.featureCarouselItem}>
                          <p className={classes.innerTitle}> {item.title} </p>
                          {/*<p className={classes.innerText}>*/}
                          {/*  {item.description}*/}
                          {/*</p>*/}
                        </div>
                      </div>
                    ))}
                  </Carousel>
                </div>
              </Hidden>
            </Grid>
            <Grid
              item
              {...{ md: 6, sm: 12 }}
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <div className={classes.mockup}>
                <div
                  className={clsx(classes.innerMockup, classes.innerMockup1)}
                ></div>
              </div>
            </Grid>
          </Grid>

          <Grid container className={classes.featureSectionYellowBox}>
            <Grid item {...{ md: 6 }}>
              <div className={classes.sectionTitle}>
                We are trying to make all of these accessible to everyone
              </div>
              <p className={classes.innerText}>
                Accessibility features are available and promoted so everyone
                can enjoy exploring, watching & shopping!
              </p>
            </Grid>

            <Grid item {...{ md: 6 }}></Grid>
          </Grid>
        </CustomContainer>
      </section>

      <section id="download" className={classes.downloadSection}>
        <CustomContainer className={classes.downloadSectionContent}>
          <img src={"/images/logo-square.png"} />

          <p className={classes.sectionTitle}>Ready for it?</p>
          <p className={classes.innerText}>
            So try this new collaborative shopping experience now!
          </p>

          <img className={"samsung"} src={"/images/samsung-galaxy-s9.png"} />
        </CustomContainer>
      </section>

      <section id="download" className={classes.sidesSections}>
        <CustomContainer>
          <p className={classes.sectionTitle}>
            A win-win solution and a great product for all parties.
          </p>
          <Grid container>
            <Grid item {...{ md: 4 }} className={classes.userBox}>
              <div>
                <img src="/images/user-icon.svg" />
                <p className={clsx(classes.innerTitle, "pink")}>For Users</p>
                <p className={classes.innerText}>
                  Have a collaborative shopping experience with Solio shoppable
                  videos. We know you may be an influencer, too.
                </p>
              </div>
            </Grid>

            <Grid item {...{ md: 4 }} className={classes.influencerBox}>
              <div>
                <img src="/images/influencer-icon.svg" />
                <p className={clsx(classes.innerTitle, "purple")}>
                  For Influencers
                </p>
                <p className={classes.innerText}>
                  Answer users in real-time, help them find their ideals, and
                  earn money while showing your favorite products.
                </p>
              </div>
            </Grid>

            <Grid item {...{ md: 4 }} className={classes.merchantBox}>
              <div>
                <img src="/images/merchants-icon.svg" />
                <p className={clsx(classes.innerTitle, "orange")}>
                  For Merchants
                </p>
                <p className={classes.innerText}>
                  Use retail technologies and tap into untapped markets without
                  a big budget.
                </p>
              </div>
            </Grid>
          </Grid>

          <div className={classes.whiteCentralTest}>
            <p className={classes.text}>Keep in Touch With Us For More Info</p>
            <div>
              <Button type={"outline"}>Contact Us</Button>
            </div>
          </div>
        </CustomContainer>
      </section>
    </div>
  );
};

export default LandingPage;

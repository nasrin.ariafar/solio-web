import React from "react";
import {__t} from "core/translation/translation";

export default [{
  title: __t("Watch"),
  path: "watch",
},{
  title: __t("Explore"),
  path: "explore",
},{
  title: __t("Shop"),
  path: "shop",
},{
  title: __t("Features"),
  path: "features",
},{
  title: __t("Download"),
  path: "download",
}]
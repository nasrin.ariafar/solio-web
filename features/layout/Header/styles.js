/**
 * @author <a href="mailto:nasrinariafar@gmail.com">Nasrin Ariafar</a>
 *  6/20/2020
 */

import {fade, makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
  root: {
    flexGrow: 1,
    display: "flex",
    alignItems: "center",

    "& header": {
      backgroundColor: "transparent !important",
      "&.solid-color": {
        backgroundColor: "white !important",
      }
    }
  },
  grow: {
    flexGrow: 1,
    height: "100%",
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    },
    "& > *": {
      fontWeight: 700,
      margin: "0 32px !important"
    },
    "& a": {
      cursor: "pointer"
    }
  },
  logo: {
    marginRight: "10px"
  },
  toolbar: {
    height: "100%",
    padding:0
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  headerMainMenuButton: {
    height: "100%",
    margin: "0 10px",
    display: "flex",
    alignItems: "center",
    // lineHeight: '55px',
    textDecoration: "none",
    "&.showActive.active": {
      // borderBottom: `1px solid ${theme.palette.secondary.main}`
    },
    "& img": {
      marginRight: "4px",
      height: "14px"
    }
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },
  sectionDesktop: {
    display: "flex",
    alignItems: "center",
    marginLeft: "auto",
    "& > a": {
      display: "flex",
      justifyContent: "center",
      margin: "0 5px"
    }
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  menuTitle: {
    padding: "0 3px",
    [theme.breakpoints.down("md")]: {
      display: "none"
    }
  },
}));

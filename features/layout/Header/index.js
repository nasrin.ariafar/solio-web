import React, {useEffect, useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import useStyles from './styles'
import clsx from "clsx";
import {Container} from "@material-ui/core";
import menuItems from "./menuItems"
import { Link, animateScroll as scroll } from "react-scroll";
import CustomContainer from "components/Container";


export default function Header() {
  const classes = useStyles();
  const [scrolled, setScrolled] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const show = window.scrollY > 100
      if (show) {
        setScrolled(true)
      } else {
        setScrolled(false)
      }
    }
    document.addEventListener('scroll', handleScroll)
    return () => {
      document.removeEventListener('scroll', handleScroll)
    }
  }, [])

  return (
    <div className={clsx(classes.root, classes.headerWrapper)}>
      <Container>
        <AppBar position="fixed" className={clsx({
          "solid-color": scrolled
        })}>
          <CustomContainer maxWidth={"lg"}>
            <Toolbar variant="dense" className={classes.toolbar}>
              <Link to={"home"}
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}>
                <img width={80} alt={"logo"} src={'/images/prototype.png'}/>
              </Link>

              <div className={classes.grow}>
                {menuItems.map(item => item.component ? <>{item.component}</> : (
                  <Link key={item.path}
                        to={item.path}
                        spy={true}
                        smooth={true}
                        offset={-70}
                        duration={500}
                        className={clsx(classes.headerMainMenuButton, {
                    showActive: item.component ? false : true
                  })}>
                    {item.title}
                  </Link>
                ))}
              </div>

              <div className={classes.sectionDesktop}>
              </div>
            </Toolbar>
          </CustomContainer>
        </AppBar>
      </Container>
    </div>
  );
}
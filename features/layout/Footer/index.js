import React from "react";
import useStyles from "./styles";
import CustomContainer from "components/Container";
import Link from 'next/link'


const Footer = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CustomContainer>
        <div>
          <div>
            <img
              alt={"logo"}
              height={20}
              style={{marginTop: "10px"}}
              src={"/images/colored-logo.svg"}
            />
          </div>
          <div>
            <span>Download Now</span>
            <span>License</span>
          </div>

          <div className={classes.innerLinks}>
            <Link href={'/'}>Homepage</Link>
            <Link href={'/contact-us'}>Contact Us</Link>
            <Link href={'/'}>Privacy Policy</Link>
          </div>

          <div className={classes.copyRight}>
            © 2020 Solio. All rights reserved
          </div>
        </div>

        <div className={classes.downloadBtn}>
          <div>
            Get the App
          </div>
          <div>
            <img src={"/images/google-play-badge.svg"}/>
          </div>
        </div>
      </CustomContainer>
    </div>
  );
};
export default Footer;

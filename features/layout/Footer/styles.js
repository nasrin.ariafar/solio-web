/**
 * @author <a href="mailto:nasrinariafar@gmail.com">Nasrin Ariafar</a>
 *  6/20/2020
 */

import {makeStyles} from "@material-ui/core";

export default makeStyles(theme => ({
  root: {
    minHeight: "280px",
    backgroundColor: theme.palette.black.main,
    color: "#FFFFFF",
    textAlign: "left",
    display: "flex",
    alignItems: "center",
    padding:"30px",
    "& div": {
      lineHeight: "26px",
      fontSize: "16px",
      marginBottom: '12px'
    },
    "& .MuiContainer-root": {
      "& > div:first-child":{
        [theme.breakpoints.down("md")]: {
          borderBottom:"1px solid #eee"
        }
      },
      "& > div:last-child":{
        marginLeft:"auto"
      },
      [theme.breakpoints.up("md")]: {
        display: "flex",
      },
    },
    "& a":{
      marginRight:"24px"
    }
  },
  downloadBtn:{

  },
  copyRight: {
    marginTop: "12px",
    color: "#D9DBE1",
    lineHeight: "24px",
    fontSize: "14px",
  },
  colContainer: {
    [theme.breakpoints.down("xs")]: {
      width: "100%"
    }
  },
  socialIconsWrapper: {
    "& img": {
      width: "20px",
      margin: "10px 15px 0 0"
    }
  },
  itemsContainer: {
    width: "300px",
    textAlign: "left",
    margin: "0 auto",
    [theme.breakpoints.down("sm")]: {
      width: "170px"
    }
  },
  centerItemsContainer: {
    width: "300px",
    textAlign: "left",
    margin: "0 auto",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    },
  },
  innerLinks:{
    "& > span":{
      marginRight:"24px"
    }
  }
}));

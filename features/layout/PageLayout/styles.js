/**
 * @author <a href="mailto:nasrinariafar@gmail.com">Nasrin Ariafar</a>
 *  6/20/2020
 */

import {makeStyles} from "@material-ui/core";


export default makeStyles(theme => ({
  root:{
    height:"100%",
    "& a":{
      textDecoration:"none",
      color:"inherit"
    }
  },
  mainContent: {
    margin:0
  },
  pageContentWrapper: {
    minHeight:"100%",
    paddingBottom:"30px",
    paddingTop:"30px",
    "&.fillWidth":{
      padding: 0
    }
  }
}));
/**
 * @author <a href="mailto:nasrinariafar@gmail.com">Nasrin Ariafar</a>
 *  9/13/2020
 */

const Layout = Object.freeze({
  FullWidth : "FULL_WIDTH",
  Central    : "CENTRAL"
});

export default Layout;
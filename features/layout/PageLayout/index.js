import React from 'react';
import useStyles from './styles'
import Header from "features/layout/Header";
import Footer from "features/layout/Footer";
import clsx from "clsx";

const PageLayout = (props) => {
  const {children} = props;
  const classes = useStyles();

  return (
    <div className={classes.root} dir={"ltr"}>
      <Header/>

      <div
        classes={clsx(classes.pageContentWrapper)}>
        {children}
      </div>

      <Footer/>
    </div>
  )
};

export default PageLayout

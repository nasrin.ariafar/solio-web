import ContactUsPage from "features/contactUs";
import PageLayout from "features/layout/PageLayout";

export default function Home() {
  return (
    <PageLayout>
      <ContactUsPage/>
    </PageLayout>
  )
}

import LandingPage from "features/landing";
import PageLayout from "features/layout/PageLayout";

export default function Home() {
  return (
    <PageLayout>
      <LandingPage/>
    </PageLayout>
  )
}

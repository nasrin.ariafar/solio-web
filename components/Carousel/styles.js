import {makeStyles} from "@material-ui/core";

export default makeStyles((theme) => ({
  customCarousel: {
    position: "relative",
  },
  carouselWrapper: {
    overflow: "hidden",
  },
  carouselInner: {
    listStyle: "none",
    display: "flex",
    margin: 0,
    padding: 0,
    overflow: "hidden",
    transition: "0.5s",
    alignItems:"center",
    justifyContent:"center"
  },
  carouselItem: {},
  arrow: {
    position: "absolute",
    top: "calc(50% - 15px)",
    cursor: "pointer",
    color: "white",
    zIndex: "100",
    width: "25px",
    height: "25px",
    borderRadius: "25px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    "&.disabled": {
      cursor: "default",
      opacity: 0.4,
    },
    "& img": {
      height: "100%"
    },
    "&.large": {
      width: "30px",
      height: "30px",
      borderRadius: "30px",
      "& svg": {
        width: "30px",
        height: "30px",
        marginTop: "10px",
      },
    },
    "&.hidden": {
      display: "none",
    },
  },
  nextArrow:
    theme.direction === "rtl"
      ? {
        right: "-15px",
        "&.large": {
          right: "0",
        },
      }
      : {
        left: "-15px",
        "&.large": {
          left: "0",
        },
      },
  prevArrow:
    theme.direction === "rtl"
      ? {
        left: "-15px",
        "&.large": {
          left: "20px",
        },
      }
      : {
        right: "-15px",
        "&.large": {
          right: "20px",
        },
      },
  bulletsWrapper: {
    // position: "absolute",
    top: "0",
    left: "0",
    right: "0",
    width: "100%",
    display: "flex",
    textAlign: "center",
    justifyContent: "center",
    "& > div":{
      width: "40px",
      height: "40px",
      background: "#F4F5F7",
      borderRadius: "100px",
      backgroundColor: "#e6e6e6",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      marginRight: "20px",
      textAlign:"center",
      opacity:"0.7",
      cursor:'pointer',
      "& img":{
        height:"100%",
        padding:"10px",
        margin:"auto"
      },
      "&.active": {
        backgroundColor: "white",
        opacity: 1
      },
    }
  },
}));

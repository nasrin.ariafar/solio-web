import React from "react";

const NextArrow = (props) => {
  const {onClick, className} = props;
  
  return (
    <div className={className} onClick={onClick}>
      <img src={'/images/arrow-left.svg'}/>
    </div>
  );
};

export default NextArrow;
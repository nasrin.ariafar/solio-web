import useStyles from "./styles";
import React from "react";

const PrevArrow = (props) => {
  const {onClick, className} = props;
  const classes = useStyles();
  return (
    <div onClick={onClick} className={className}>
      <img src={'/images/arrow-right.svg'}/>
    </div>
  );
};

export default PrevArrow;
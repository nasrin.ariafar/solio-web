import React from "react";
import useStyles from "./BulletItemStyles";
import clsx from "clsx";

const BulletItem = ({ item, index, isActive = false, onClick }) => {
  const classes = useStyles(item);
  return (
    <span
      onClick={() => onClick(index)}
      className={clsx(classes.root, {
        active: isActive,
      })}
    />
  );
};

export default BulletItem;

import { makeStyles } from "@material-ui/styles";

export default makeStyles((theme) => ({
  root: (item) => ({
    width: "14px",
    height: "8px",
    borderRadius: "5px",
    margin: "0 5px",
    cursor: "pointer",
    display: "inline-block",
    backgroundColor: "#e6e6e6",
    "&.active": {
      width: "30px",
      borderRadius: "5px",
      backgroundColor: item.color,
    },
  }),
}));

import React, {useEffect, useRef, useState} from "react";
import useStyles from "./styles";
import clsx from "clsx";
import PropTypes from "prop-types";
import PrevArrow from "./PrevArrow";
import NextArrow from "./NextArrow";
import BulletItem from "./bullet/BulletItem";
import {useSwipeable} from "react-swipeable";


const Carousel = (props) => {
  const {
    items = [], itemWidth, itemsToShow,
    direction = "ltr",
    showExtra = true,
    autoPlay = true,
    onChangeSlide
  } = props;
  const itemsCount = items.length;
  const [activeIndex, setActiveIndex] = useState(props.activeIndex || 0);
  const classes = useStyles();
  const interval = useRef();
  const showArrows = itemsCount > itemsToShow ? true : false;
  const factor = direction === "ltr" ? -1 : 1;
  const delta = 0;// !showExtra ? 0 : itemsCount;
  const disableNext = (activeIndex + itemsToShow) === itemsCount ? true : false

  useEffect(() => {
    onChangeSlide && onChangeSlide(activeIndex)
  }, [activeIndex])

  useEffect(() => {
    if (autoPlay) {
      interval.current = setInterval(() => {
        changeActiveIndexAutomatically();
      }, 6000);
    }

    return () => interval.current && clearInterval(interval.current);
  }, []);

  const changeActiveIndexAutomatically = () => {
    setActiveIndex(activeIndex => activeIndex < itemsCount - 1 ? activeIndex + 1 : 0);
  };

  /**
   * First item add to the end of items
   */
  const onNext = (e) => {
    if (activeIndex + itemsToShow === itemsCount) {
      return;
    }
    const index = (activeIndex + itemsToShow <= itemsCount - itemsToShow) ? activeIndex + itemsToShow : activeIndex + 1;
    console.log(index, itemsCount)
    const newIndex = index % itemsCount;
    setActiveIndex(newIndex);

    clearInterval(interval.current);
    e && e.preventDefault();
  };

  /**
   * Last item add to at the first of items
   */
  const onPrevious = (e) => {
    if (activeIndex == 0) {
      return;
    }
    const index = activeIndex - itemsToShow;
    const newIndex = index < 0 ? 0 : index;
    setActiveIndex(newIndex);
    clearInterval(interval.current);
    e && e.preventDefault();
  };

  const onBulletClick = (index) => {
    setActiveIndex(index);
  };

  const onSwipedRight = ({event}) => {
    if (direction === "ltr") {
      onPrevious(event)
    } else {
      onNext(event)
    }
  }

  const onSwipedLeft = ({event}) => {
    if (direction === "ltr") {
      onNext(event)
    } else {
      onPrevious(event)
    }
  };

  const handlers = useSwipeable({
    onSwipedLeft,
    onSwipedRight,
    preventDefaultTouchmoveEvent: true
  });

  return (
    <>
      {itemsToShow === 1 && (
        <div className={classes.bulletsWrapper}>
          {items.map((item, index) => (
            <div
              index={index}
              className={clsx({"active": index === activeIndex})}
              onClick={() => onBulletClick(index)}
              key={index}
            >
              {item.icon}
            </div>
          ))}
        </div>
      )}
      <div {...handlers}>
        <div className={classes.customCarousel} dir={direction}>
          <div className={classes.carouselWrapper}>
            <div
              className={clsx("carouselInner", classes.carouselInner)}
              style={{
                width: `${(itemWidth * itemsCount) - (disableNext ? 0 : delta)}%`,
                transform: `translateX(${
                  ((factor * activeIndex) / itemsCount) * 100
                }%)`,
                WebkitTransform: `translateX(${
                  ((factor * activeIndex) / itemsCount) * 100
                }%)`,
                MsTransform: `translateX(${
                  ((factor * activeIndex) / itemsCount) * 100
                }%)`,
              }}
            >
              {props.children}
            </div>
          </div>

          {showArrows && (
            <>
              <PrevArrow
                onClick={onPrevious}
                className={clsx(classes.arrow, classes.prevArrow, {
                  disabled: activeIndex == 0 ? true : false,
                  large: itemsToShow === 1,
                })}
              />

              <NextArrow
                onClick={onNext}
                className={clsx(classes.arrow, classes.nextArrow, {
                  disabled: disableNext,
                  large: itemsToShow === 1,
                })}
              />
            </>
          )}

        </div>
      </div>
    </>
  );
};

Carousel.propTypes = {
  itemWidth: PropTypes.number,
  itemsToShow: PropTypes.number,
  onChangeSlide: PropTypes.func
};

export default Carousel;

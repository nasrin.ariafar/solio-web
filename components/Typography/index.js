import React, {useMemo} from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import MuiTypography from '@material-ui/core/Typography';
import mapValues from "core/helper/mapValues";
import getObjectValueByString from "core/helper/getObjectValueByString";
import clsx from "clsx";
import theme from "core/style/theme";

const variantMapping = {
  '2xl': {
    element: 'p',
    styles: {
      primary: {
        fontSize: 34,
        lineHeight: '150%',
        letterSpacing: '1.5px',
      },
      secondary: {
        fontSize: 34,
        lineHeight: '150%',
        letterSpacing: "1.5px",
      },
    },
  },
  xl: {
    element: 'p',
    styles: {
      primary: {
        fontSize: 24,
        lineHeight: '35px',
        letterSpacing: '1.2px',
      },
      secondary: {
        fontSize: 24,
        lineHeight: '35px',
        letterSpacing: "1.2px",
      },
    },
  },
  lg: {
    element: 'p',
    styles: {
      primary: {
        fontSize: "20px",
        lineHeight: 1.75,
        letterSpacing: 0,
      },
      secondary: {
        fontSize: "20px",
        lineHeight: 1.75,
        letterSpacing: 0,
      },
    },
  },
  md: {
    element: 'p',
    styles: {
      primary: {
        fontSize: 16,
        lineHeight: '130%',
      },
      secondary: {
        fontSize: 16,
        lineHeight: '140%',
      },
    },
  },
  sm: {
    element: 'p',
    styles: {
      primary: {
        fontSize: 14,
        lineHeight: '140%',
        letterSpacing: 0,
      },
      secondary: {
        fontSize: 14,
        lineHeight: '140%',
        letterSpacing: 0,
      },
    },
  },
  xs: {
    element: 'span',
    styles: {
      primary: {
        fontSize: 13,
        lineHeight: '140%',
        letterSpacing: 0,
      },
      secondary: {
        fontSize: 13,
        lineHeight: '140%',
        letterSpacing: 0,
      },
    },
  },
  '2xs': {
    element: 'span',
    styles: {
      primary: {
        fontSize: 12,
        lineHeight: '160%',
        letterSpacing: 0,
      },
      secondary: {
        fontSize: 12,
        lineHeight: '160%',
        letterSpacing: 0,
      },
    },
  },
  '3xs': {
    element: 'span',
    styles: {
      primary: {
        fontSize: 10,
        lineHeight: '140%',
        letterSpacing: 0,
      },
      secondary: {
        fontSize: 10,
        lineHeight: '140%',
        letterSpacing: 0,
      },
    },
  },
};

const useStyles = makeStyles(theme => ({
  root: ({ variant, isBold, font, color, spacing, capitalize }) => {
    const baseStyles = {
      color: !!color ?
        getObjectValueByString(
          theme.palette,
          color.split('.').length === 1 ? `${color}.main` : color)
        : theme.palette.textColor,
      
      fontFamily: isBold ? "SolioBold" : "Solio",
      fontWeight: isBold ? 500 : "normal",
      letterSpacing: "2px !important"//spacing ? `${spacing}px !important` : 0
    };
    const variantStyles = variantMapping[variant].styles[font];
    return {
      ...baseStyles,
      ...variantStyles,
      letterSpacing: spacing ? `${spacing}px` : variantStyles.letterSpacing,
      textTransform: capitalize ? "capitalize" : "none"
    };
  },
}));

function Typography({ children, variant="xs", className, capitalize, component, isBold, noWrap, gutterBottom, font, color, spacing }) {
  const classes = useStyles({ variant, isBold, noWrap, font, color, spacing });
  const text = useMemo(() => {
    if(capitalize){
      return Array.isArray(children) ? children.map(child => child.replace(/_/g, " ")) : children.replace(/_/g, " ")
    }
    return  children;
  }, [children])

  return (
    <MuiTypography
      classes={classes}
      variantMapping={mapValues(variantMapping, function (obj) {
        return obj["element"]
      })}
      variant={variant}
      component={component}
      className={clsx(className, {
        capitalize
      })}
      color={color}
      noWrap={noWrap}
      gutterBottom={gutterBottom}
    >
      { text }
    </MuiTypography>
  );
}

Typography.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.shape({
    root: PropTypes.string.isRequired,
  }),
  // variant: PropTypes.oneOf(Object.keys(MuiTypography.va)).isRequired,
  component: PropTypes.string,
  className: PropTypes.string,
  isBold: PropTypes.bool,
  noWrap: PropTypes.bool,
  gutterBottom: PropTypes.bool,
  spacing: PropTypes.number,
  font: PropTypes.oneOf(['primary', 'secondary']),
  color: PropTypes.oneOf(Object.keys(theme.palette)),
  capitalize: PropTypes.bool
};

Typography.defaultProps = {
  isBold: false,
  component: null,
  className: '',
  font: 'primary',
  color: 'textColor',
  noWrap: false,
  gutterBottom: false,
  spacing:0,
  capitalize: false
};

export default Typography;
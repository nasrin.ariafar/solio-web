import React, { memo } from "react";
import useStyles from "./styles";
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";

function WaitLayout({ hasBackground = false, color="secondary" }) {
  const classes = useStyles({ hasBackground });
  return (
    <div className={classes.root}>
      <CircularProgress color={color}/>
    </div>
  );
}

export default memo(WaitLayout);

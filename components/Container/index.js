import React from 'react';
import Container from "@material-ui/core/Container/Container";

const CustomContainer = ({children, maxWidth, classes='', style={}}) => {
  return(
    <Container maxWidth={maxWidth ?? "lg"} className={classes} style={style}>
      { children }
    </Container>
  )
};


export default CustomContainer
import palette from "./palette";

export default {
  MuiCssBaseline: {
    '@global': {
      '& img[src=""]': {
        display: "none"
      },
    },
  },
  MuiInput: {
    underline: {
      borderColor: "#ffffffb3 !important",
      borderBottom: "1px solid #ffffffb3",
      "&:after": {
        borderColor: "#ffffffb3 !important",
        borderBottom: "1px solid #ffffffb3"
      },
    }
  },
  MuiButton: {
    root: {
      boxShadow: "none !important",
      textTransform: "none"
    },
    containedPrimary: {
      backgroundColor: "#222222",
      color: "white"
    },
    sizeLarge:{
      "& $label":{
        fontSize:"16px",
        fontWeight:500
      }
    },
    outlined:{
      borderColor:"#222222"
    },
    outlinedSizeLarge:{
      padding:"6px 21px"
    }
  },
  MuiIconButton: {
    root: {
      "&:hover": {
        backgroundColor: "transparent !important"
      }
    }
  },
  MuiDialog: {
    container: {
      direction: "ltr"
    }
  },
  MuiMenuItem: {
    root: {
      "&$selected": {
        color: "#0d1013",
      }
    }
  },
  MuiRadio: {
    root: {
      color: "#FFFFFF",
      '&$checked': {
        color: "#0034a4"
      },
    }
  },
  MuiTypography: {
    root: {
      margin: "10px 0"
    }
  },
  MuiList: {
    root: {
      padding: "20px"
    }
  },
  MuiLinearProgress: {
    barColorPrimary: {
      backgroundColor: palette.blue.main,
    },
    colorPrimary: {
      backgroundColor: palette.blue.light
    }
  },
  MuiStepIcon: {
    root: {
      width: 30,
      height: 30,
      color: "transparent !important",
      border: `1px solid ${palette.gray.main}`,
      borderRadius: "50%",
      "& text": {
        fill: palette.gray.main + " !important"
      }
    },
    completed: {
      border: `1px solid ${palette.background.default}`,
      backgroundColor: palette.background.default + " !important",
      color: palette.textColor + " !important"
    },
    active: {
      border: `1px solid ${palette.background.default}`,
      color: palette.secondary.main + " !important",
      "& text": {
        fill: "white !important"
      }
    }
  },
  MuiAppBar: {
    root: {
      height: "55px",
      boxShadow: "none"
    }
  },
  MuiInputBase: {
    root: {
      height: "42px",
    },
    input: {
      height: "32px",
      lineHeight: "28px",
    },
    multiline:{
      minHeight: 150
    }
  },
  MuiOutlinedInput: {
    notchedOutline: {
      border: `1px solid ${palette.gray.main} !important`
    },
  },
  MuiToolbar: {
    regular: {
      minHeight: "100% !important"
    }
  },
  MuiTab:{
    root:{
      textTransform:"none"
    }
  },
  MuiChip: {
    root: {
      borderRadius: "5px"
    },
    colorPrimary: {
      color: "white",
      backgroundColor: "#22222273",
    }
  },
  MuiBox:{
    root:{
      padding: 0
    }
  },
  MuiSwitch:{
    root:{
      width: "65px",
      height: "44px"
    },
    switchBase:{
      top: "5px",
      right: "20px"
    },
    thumb:{
      width: "15px",
      height: "15px",
      borderRadius:"4px",
      backgroundColor:"white",
      "& $checked":{
        backgroundColor: palette.secondary.main
      }
    },
    colorSecondary:{
      "&$checked + $track":{
        opacity:1
      }
    }
  },
  PrivateTabIndicator:{
    colorPrimary:{
      backgroundColor: palette.primary.contrastText,
      boxShadow: "0 3px 6px 0 #222222",
      height:"1px"
    }
  }
}
export default {
  type: "light",
  primary: {
    main: "#FFFFFF",
    dark: "#0d1013",
    contrastText: "#222222"
  },
  secondary: {
    light: "#e80b2c",
    main: "#b90722",
    dark: "#98041b",
    contrastText: "#FFFFFF"
  },
  error: {
    light: "#c6611b",
    main: "#c6372c",
    dark: "#c6372c",
    requiredQuiz: "#db3337",
    chipBackground: "#FFE6E7"
  },
  success: {
    light: "#51C768",
    main: "#3CBA54",
    dark: "#34A048",
    chipBackground: "#E1F5E5"
  },
  warning: {
    light: "#F5CA2D",
    main: "#F4C20D",
    dark: "#D5A90A",
    chipColor: "#F89239",
    chipBackground: "#FDEEE2"
  },
  gray: {
    light: "#f6f6f6",
    main: "#22222259",
    dark: "#858585",
    subtitle: "#22222259",
    background: "#eeeeee",
    mediumGray: "rgba(34, 34, 34, 0.35)"
  },
  pink:{
    main:"#F22BB2"
  },
  purple:{
    main:"#4945FF"
  },
  timeLine: {
    circleBackgroundColor: "#ffffff80",
    lineBackgroundColor: "#0d1013"
  },
  grayBlue: {
    light: "#e2e6f0",
    main: "#8CA4B9",
    dark: "#333e4b",
    darker: "#607D8B",
    a2: "#a2b8c3",
    filename: "#6489a0",
    closeButton: "#a1b5c1",
    totalCount: "#607d8b",
    borderBottom: "#8ca4b9",
    menuOpen: "#95A1A7",
    d4: "#D4DDE2"
  },
  black: {
    light: "#18191F",
    main: "#000000",
    dark: "#000000",
    feedback: "#212121"
  },
  yellow: {
    main: "#FFD701",
    dark: "#de8900",
    fd: "#fdf5d5",
    e3: "#e3b409"
  },
  orange: {
    light: "#F47645",
    fd: "#fdba80",
    main:"#FF7A00"
  },
  red: {
    light: "#E04F52",
    main: "#e02020",
    dark: "#C82327",
    f0: "#f0acae",
    c6: "#c61717",
    d8: "#d83236",
    d0: "#d00303",
    b4: "#b41c1f",
    ff: "#FFECED"
  },
  blue: {
    main: "#4945FF",
    dark: "#2c58c6",
    light1: "#A0DCFF",
    light2: "#C2F0FF",
  },
  background: {
    default: "#FFFFFF",
    paper: "#FFFFFF"
  },
  textColor: "#222222",
  toolbar: {
    color: "#A1B5C6",
    height: "75px"
  },
  toggleButton: { selected: "#D4DBE1" },
  filterBackground: "#E9ECEF",
  groupsDivider: "#c6c6c6",
  multiImage: {
    light: "#A3A3A3",
    dark: "#838383"
  },
  green: {
    light: "#def0e1",
    main: "#6bc077",
    dark: "#23A393",
    completeThisCourse: "#4ACEBD",
    quizScore: "#76cf87",
    checkCircle: "#8bc34a",
    withoutAssessmentChipBackground: "#C4FAF4",
    withoutAssessmentChipColor: "#119382",
    completedPercentage: "#34a7b2",
    available:"#4cc0bf"
  },
  customButton: {
    main: "#6bc077",
    textColor: "#0d1013",
    hover: "#4eb15c",
    disabled: "#6bc0778f"
  },
  progressBar: { background: "#F0F0F0" },
  plan:{
    free: "#999999",
    lite: "#4cc0bf",
    pro:"#fdcc1f",
    amateur:"#29cc92",
  }
};

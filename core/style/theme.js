import {createMuiTheme, responsiveFontSizes} from '@material-ui/core/styles';
import palette from "core/style/palette";
import overrides from "core/style/overrides";



const fontSize = {
  10: `62.50%`,   // 0.65rem   || 10px
  11: `68.75%`,   // 0.7rem   || 11px
  12: `75.00%`,   // 0.8rem   || 12px
  14: `87.50%`,  // 0.87rem  || 14px
  15: `93.75%`,   // 0.9rem  || 15px
  16: `100%`,   // 1rem  || 16px
  20: `125.00%`,   // 1.25rem  || 20px
  37: `231.25%`,   // 1.3rem  || 37px
};


const theme = createMuiTheme({
  // breakpoints: {
  //   values: {
  //     xs: 0,
  //     sm: 576,
  //     md: 768,
  //     lg: 992,
  //     xl: 1160,
  //     xxl: 1600
  //   }
  // },
  fontSize: fontSize,
  spacing: 2,
  direction: "ltr",
  distance: "30px",
  palette,
  overrides ,
});


export default responsiveFontSizes(theme);

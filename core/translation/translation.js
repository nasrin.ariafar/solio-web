import en_message from "./locales/en.json";
export const __t = (str, values) => {
  // const intl = useIntl();
  if(!!str) {

    return en_message[str] ?? str
  }
  return ""
};

